# NAVAL

## Getting started

Naval is a project that helps to back up and share photos.

## Design decisions

Naval is a microservice based UI.

- It does not use cookies. Customers will notice your brand right away by avoiding the distactions of the legally enforced 'Accept Cookie' buttons.
- It is experimental. We need to improve latency for example. It was not designed for animation but forms.
- It is still running in the browser, you do not need app store approvals, etc.
Those improve app quality, but they can be a hassle for experimental or internal projects.
- It is Creative Commons Zero licensed. Take the code, change it, use it, keep your changes secret, we do not care. Make sure that you review what you release. Reliability, any patent infringement, etc. is your responsibility.

## How to use

Download the sources
```
git clone https://gitlab.com/eper.io/naval.git
```

Run the project
```
go run ./main.go
```

Check the site and log in with the activation key: `THGABTGADSSIEOGSEKAEMHDCJEABLBEPASTRROCRDGQAGJQISBTSTQFPAKRANTKA`
```
http://127.0.0.1:7777/
```

Bookmark the management url that shows up. This is your secret to get site logs for example: `ICSABAJFFLDPPFEDPGDHETTBTSJGLEOPSJTNHCRLJGANIFFIGCHFHADGDIDFDNMS`
```
http://127.0.0.1:7777/management.html?apikey=ICSABAJFFLDPPFEDPGDHETTBTSJGLEOPSJTNHCRLJGANIFFIGCHFHADGDIDFDNMS
```

You can give the survey link to your clients. Replace the api key with the real one generated from your running instance in the browser.
Users with this link and key can upload, see the photos, like them, but they cannot manage the site.
```
http://127.0.0.1:7777/photos.html?apikey=THGABTGADSSIEOGSEKAEMHDCJEABLBEPASTRROCRDGQAGJQISBTSTQFPAKRANTKA
```

Make sure you use the load balancer with TLS of your cloud provider.
The final URL should look like:
```
https://yourdomain.example.com/photos.html?apikey=THGABTGADSSIEOGSEKAEMHDCJEABLBEPASTRROCRDGQAGJQISBTSTQFPAKRANTKA
```

## Docker images

Here are some docker images to play with
```
docker pull registry.gitlab.com/eper.io/naval:latest
docker pull eperio/naval
```

## License

This document is Licensed under Creative Commons CC0. To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this document to the public domain worldwide. This document is distributed without any warranty. You should have received a copy of the CC0 Public Domain Dedication along with this document. If not, see <https://creativecommons.org/publicdomain/zero/1.0/legalcode>.  