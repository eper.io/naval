package photos

import (
	"bufio"
	"fmt"
	"gitlab.com/eper.io/naval/drawing"
	"io"
	"net/http"
	"os"
	"path"
	"strings"
)

// Display is a single display card
type Display struct {
	Key      string // Private key of display
	Media    string // Media file to display
	Href     string // URL That Media file points to
	Reported bool   // If the deal is reported as inappropriate
	Status   string // Any status reported
}

var photos = make(map[string]*Display)

// A random order of a specified view
var sessions = map[string][]card{}

func callEnglang(snapshot string) error {
	if callEnglangDisplaySlot(strings.NewReader(snapshot)) == nil {
		return nil
	}
	return fmt.Errorf("do not understand")
}

func callEnglangCreateFile(name string, snapshot io.Reader) string {
	dst := GenerateLocalFileName(name)
	_, _ = io.Copy(drawing.NoErrorFile(os.Create(dst)), snapshot)
	return dst
}

func GenerateLocalFileName(key string) string {
	wwwRoot := "/tmp/wwwroot"
	_ = os.Mkdir(wwwRoot, 0700)
	wwwRoot = "/tmp/wwwroot/photos"
	_ = os.Mkdir(wwwRoot, 0700)

	png := fmt.Sprintf("/%s.png", drawing.RedactPublicKey(key))
	dst := path.Join(wwwRoot, png)
	return dst
}

func callEnglangDisplaySlot(snapshot io.Reader) error {
	card := &Display{Key: "?", Media: "", Href: "/"}
	n, err := fmt.Fscanf(snapshot, "Create photo with key %s pointing to %s with image %s", &card.Key, &card.Href, &card.Media)
	if err != nil {
		return err
	}
	if n != 3 {
		return fmt.Errorf("englang parsing error display")
	}

	wwwRoot := "/tmp/wwwroot"
	_ = os.Mkdir(wwwRoot, 0700)
	wwwRoot = "/tmp/wwwroot/photos"
	_ = os.Mkdir(wwwRoot, 0700)

	png := fmt.Sprintf("/%s.png", drawing.RedactPublicKey(card.Key))
	dst := path.Join(wwwRoot, png)

	var reader io.Reader
	if strings.HasPrefix(card.Media, "./photos/res") {
		reader = drawing.NoErrorFile(os.Open(card.Media))
		_, _ = io.Copy(drawing.NoErrorFile(os.Create(dst)), reader)
		card.Media = dst
	} else if !strings.HasPrefix(card.Media, "/tmp/wwwroot/photos") {
		// already in place
	} else if strings.HasPrefix(card.Media, "https://") {
		client := http.Client{}
		resp, err := client.Get(card.Media)
		if err == nil {
			_, _ = io.Copy(drawing.NoErrorFile(os.Create(dst)), resp.Body)
			card.Media = dst
		}
	}

	photos[card.Key] = card
	return nil
}

func SetupTestData() {
	// TODO load from Redis here
	reader := drawing.NoErrorFile(os.Open("./photos/res/testdata.txt"))
	defer reader.Close()
	scanner := bufio.NewScanner(reader)
	for scanner.Scan() {
		err := callEnglang(scanner.Text())
		if err != nil {
			return
		}
	}
}

func DebuggingInformation(w http.ResponseWriter, r *http.Request) {
	for _, v := range sessions {
		_, _ = w.Write([]byte(fmt.Sprintf("%s\n", v)))
	}
	return
}
