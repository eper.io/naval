package photos

import (
	"bytes"
	"fmt"
	"gitlab.com/eper.io/naval/drawing"
	"gitlab.com/eper.io/naval/management"
	"gitlab.com/eper.io/naval/metadata"
	"image"
	"image/jpeg"
	"image/png"
	"math/rand"
	"net/http"
	"os"
	"strings"
)

// Copyright Schmied Enterprises LLC

type card interface {
	IsReported() bool
	Report()
	GetKey() string
	GetMedia() string
	GetReference() string
	IsApproved() bool
	SetApproved(bool bool)
}

func (d *Display) GetMedia() string {
	return d.Media
}

func (d *Display) GetKey() string {
	return d.Key
}

func (d *Display) GetReference() string {
	return d.Href
}

func (d *Display) IsReported() bool {
	return d.Reported
}

func (d *Display) Report() {
	d.Reported = !d.Reported
}

func (d *Display) IsApproved() bool {
	return strings.Contains(d.Status, "card is liked.")
}

func (d *Display) SetApproved(b bool) {
	if b != d.IsApproved() {
		if b {
			d.Status = "card is liked."
		} else {
			d.Status = ""
		}
	}
}

func SetupPhotos() {
	// TODO Load from Redis
	if strings.Contains(metadata.CompanyName, "(SAMPLE)") {
		SetupTestData()
	}

	http.HandleFunc("/photos.html", func(w http.ResponseWriter, r *http.Request) {
		_, err := management.EnsureAdministrator(w, r)
		if err != nil {
			return
		}
		if drawing.ResetSession(w, r) != nil {
			return
		}
		drawing.ServeRemoteForm(w, r, "photos")
	})
	http.HandleFunc("/photos.png", func(w http.ResponseWriter, r *http.Request) {
		_, err := management.EnsureAdministrator(w, r)
		if err != nil {
			return
		}
		drawing.ServeRemoteFrame(w, r, declarePhotosForm)
	})
	for _, v := range photos {
		UpdateLinkOf(v)
	}

}

func UpdateLinkOf(d *Display) {
	path := fmt.Sprintf("/%s.png", d.Key)
	http.HandleFunc(path, func(w http.ResponseWriter, r *http.Request) {
		f := GenerateLocalFileName(path)
		http.ServeFile(w, r, f)
	})
}

func declarePhotosForm(session *drawing.Session) {
	// Create a fixed order
	cardsInit, ok := sessions[session.ApiKey]
	if !ok || cardsInit == nil {
		cardsInit = make([]card, 0)
		cardsInit = append(cardsInit, addUploadButton())

		for k := range photos {
			cardsInit = append(cardsInit, photos[k])
		}

		rand.Shuffle(len(cardsInit), func(i, j int) {
			cardsInit[i], cardsInit[j] = cardsInit[j], cardsInit[i]
		})

		sessions[session.ApiKey] = cardsInit
	}

	declareListForm(session, "./photos/res/list.png")
}

func addUploadButton() card {
	return &Display{Media: "./photos/res/upload.png", Href: "<upload>", Key: drawing.GenerateUniqueKey()}
}

func declareListForm(session *drawing.Session, background string) {
	if session.Form.Boxes == nil {
		const LikeButton = 0
		const ReportButton = 1
		const LinkButton = 2
		const LeftButton = 15
		const RightButton = 16
		const PageNumber = 23
		drawing.DeclareForm(session, background)
		drawing.DeclareImageField(session, LikeButton, "./photos/res/like.png", drawing.ActiveContent{Text: "", Lines: 3, Editable: false, FontColor: drawing.White, BackgroundColor: drawing.Black, Alignment: 1})
		drawing.DeclareImageField(session, ReportButton, "./photos/res/reported.png", drawing.ActiveContent{Text: "", Lines: 3, Editable: false, FontColor: drawing.White, BackgroundColor: drawing.Black, Alignment: 1})
		drawing.DeclareImageField(session, LinkButton, "./photos/res/link.png", drawing.ActiveContent{Text: "", Lines: 3, Editable: false, FontColor: drawing.White, BackgroundColor: drawing.Black, Alignment: 1})
		drawing.DeclareImageField(session, LeftButton, "./photos/res/moveleft.png", drawing.ActiveContent{Text: "", Lines: 3, Editable: false, FontColor: drawing.White, BackgroundColor: drawing.Black, Alignment: 1})
		drawing.DeclareImageField(session, RightButton, "./photos/res/moveright.png", drawing.ActiveContent{Text: "", Lines: 3, Editable: false, FontColor: drawing.White, BackgroundColor: drawing.Black, Alignment: 1})
		boxes := []int{3, 5, 7, 9, 11, 13, 17, 19, 21}
		var cardOfBox = map[int]int{3: 0, 5: 1, 7: 2, 9: 3, 11: 4, 13: 5, 17: 6, 19: 7, 21: 8}
		session.SelectedBox = boxes[0]
		session.SignalFocusChanged = func(session *drawing.Session, switchFrom int, switchTo int) {
			cards := sessions[session.ApiKey]
			found := false
			for card, x := range boxes {
				if switchTo == x && session.BaseIndex+card < len(cards) {
					found = true
				}
			}

			_, isCard := cardOfBox[switchFrom]
			if switchFrom != -1 && isCard {
				formatted := session.Text[switchFrom+1]
				formatted.Background = drawing.NewImageSliceFromPng("./photos/res/notselected.png")
				session.Text[switchFrom+1] = formatted
				session.SignalPartialRedrawNeeded(session, switchFrom+1)
			}

			if found {
				formatted := session.Text[switchTo+1]
				formatted.Background = drawing.NewImageSliceFromPng("./photos/res/selected.png")
				session.Text[switchTo+1] = formatted
				session.SignalPartialRedrawNeeded(session, switchTo+1)
				session.SignalPartialRedrawNeeded(session, LikeButton)
			} else {
				session.SignalClicked(session, switchTo)
			}
		}
		session.SignalUploaded = func(session *drawing.Session, upload drawing.Upload) {
			for _, v := range boxes {
				if session.Text[v].BackgroundFile == "./photos/res/wait.png" {
					drawing.DeclareImageField(session, v, "./photos/res/upload.png", drawing.ActiveContent{Text: "", Lines: 3, Selectable: false, Editable: false, FontColor: drawing.White, BackgroundColor: drawing.Black, Alignment: 1})
					//session.SignalPartialRedrawNeeded(session, k)
				}
			}
			session.Upload = ""
			in := bytes.NewBuffer(upload.Body)
			img, err := jpeg.Decode(in)
			if err != nil {
				session.SignalRecalculate(session)
				fmt.Println(err)
				return
			}
			buf := bytes.NewBuffer([]byte{})
			err = png.Encode(buf, img)
			if err != nil {
				session.SignalRecalculate(session)
				fmt.Println(err)
				return
			}
			key := drawing.GenerateUniqueKey()
			photoPath := fmt.Sprintf("/%s.png", key)
			final := callEnglangCreateFile(photoPath, buf)
			d := &Display{Media: final, Href: photoPath, Key: key}
			err = callEnglang(fmt.Sprintf("Create photo with key %s pointing to %s with image %s\n",
				d.Key,
				d.Href,
				d.Media))
			if err != nil {
				session.SignalRecalculate(session)
				fmt.Println(err)
				return
			}
			UpdateLinkOf(d)

			cards := sessions[session.ApiKey]
			cardsNew := make([]card, len(cards)+1)
			cardsNew[0] = cards[0]
			cardsNew[1] = d
			copy(cardsNew[2:], cards[1:])
			sessions[session.ApiKey] = cardsNew
			for k, v := range boxes {
				if session.Text[v].BackgroundFile == "./photos/res/wait.png" {
					drawing.DeclareImageField(session, v, "./photos/res/upload.png", drawing.ActiveContent{Text: "", Lines: 3, Selectable: false, Editable: false, FontColor: drawing.White, BackgroundColor: drawing.Black, Alignment: 1})
					session.SignalPartialRedrawNeeded(session, k)
				}
			}
			session.SignalRecalculate(session)
		}
		session.SignalClicked = func(session *drawing.Session, i int) {
			cards := sessions[session.ApiKey]
			for card, x := range boxes {
				if i == x && session.BaseIndex+card < len(cards) {
					if cards[session.BaseIndex+card].GetMedia() == "./photos/res/upload.png" {
						session.Upload = "image/jpeg,image/gif"
						drawing.DeclareImageField(session, i, "./photos/res/wait.png", drawing.ActiveContent{Text: "", Lines: 3, Selectable: false, Editable: false, FontColor: drawing.White, BackgroundColor: drawing.Black, Alignment: 1})
						session.SignalPartialRedrawNeeded(session, i)
						return
					}
				}
			}

			if i == LikeButton {
				if session.SelectedBox != -1 {
					i := cardOfBox[session.SelectedBox] + session.BaseIndex

					if i != -1 && i < len(cards) {
						if cards[i].GetMedia() != "./photos/res/upload.png" {
							if cards[i].IsApproved() {
								cards[i].SetApproved(false)
							} else {
								cards[i].SetApproved(true)
							}
						}
					}
					session.SignalRecalculate(session)
				}
			}
			if i == ReportButton {
				if session.SelectedBox != -1 {
					index := cardOfBox[session.SelectedBox] + session.BaseIndex
					cards[index].Report()
					session.SignalRecalculate(session)
				}
			}
			if i == LinkButton {
				if session.SelectedBox != -1 {
					index := cardOfBox[session.SelectedBox] + session.BaseIndex
					session.Redirect = cards[index].GetReference()
				}
				return
			}
			if i == LeftButton {
				session.BaseIndex = session.BaseIndex - 9
				if session.BaseIndex < 0 {
					// TODO =0?
					session.BaseIndex = session.BaseIndex + 9
				} else {
					session.SelectedBox = boxes[0]
				}
				session.SignalRecalculate(session)
			}
			if i == RightButton {
				session.BaseIndex = session.BaseIndex + 9
				if session.BaseIndex > len(cards)-1 {
					session.BaseIndex = session.BaseIndex - 9
				} else {
					session.SelectedBox = boxes[0]
				}
				session.SignalRecalculate(session)
			}
		}
		session.SignalRecalculate = func(session *drawing.Session) {
			cards := sessions[session.ApiKey]
			pageNumber := session.Text[PageNumber].Text
			newPageNumber := fmt.Sprintf("%d of %d", session.BaseIndex/9+1, (len(sessions[session.ApiKey])+8)/9)
			if newPageNumber != pageNumber {
				drawing.DeclareTextField(session, PageNumber, drawing.ActiveContent{Text: newPageNumber, Lines: 1, Editable: false, FontColor: drawing.White, BackgroundColor: drawing.Black, Alignment: 0})
				session.SignalPartialRedrawNeeded(session, PageNumber)
			}
			for _, x := range boxes {
				if session.SelectedBox == x {
					drawing.DeclareImageField(session, x+1, "./photos/res/selected.png", drawing.ActiveContent{Text: "", Lines: 3, Editable: false, FontColor: drawing.White, BackgroundColor: drawing.Black, Alignment: 1})
				} else {
					drawing.DeclareImageField(session, x+1, "./photos/res/notselected.png", drawing.ActiveContent{Text: "", Lines: 3, Editable: false, FontColor: drawing.White, BackgroundColor: drawing.Black, Alignment: 1})
				}
				session.SignalPartialRedrawNeeded(session, x+1)
			}
			for card, i := range boxes {
				card = card + session.BaseIndex
				if card < 0 || card >= len(cards) {
					drawing.DeclareImageField(session, i, "drawing/res/space.png", drawing.ActiveContent{Text: "", Lines: 3, Editable: false, FontColor: drawing.White, BackgroundColor: drawing.Black, Alignment: 1})
					session.SignalPartialRedrawNeeded(session, i)
					continue
				}
				if cards[card].IsReported() {
					drawing.DeclareImageField(session, i, "./photos/res/reported.png", drawing.ActiveContent{Text: "", Lines: 3, Selectable: true, Editable: false, FontColor: drawing.White, BackgroundColor: drawing.Black, Alignment: 1})
					session.SignalPartialRedrawNeeded(session, i)
					continue
				}

				// Reset and set markers
				media := cards[card].GetMedia()
				drawing.DeclareImageField(session, i, media, drawing.ActiveContent{Text: "", Lines: 3, Selectable: true, Editable: false, FontColor: drawing.White, BackgroundColor: drawing.Black, Alignment: 1})
				updateCardWithMarker(session, i, cards[card])
				session.SignalPartialRedrawNeeded(session, i)
				continue
			}
		}
		session.SignalClosed = func(session *drawing.Session) {
			session.SelectedBox = -1
		}
		session.SignalRecalculate(session)
	}
}

func updateCardWithMarker(session *drawing.Session, i int, card card) {
	formatted := session.Text[i]
	formatted.Background = drawing.NewImageSliceDuplicated(formatted.Background)

	var icon image.Image = nil
	if card.IsApproved() {
		icon = drawing.NoErrorImage(png.Decode(drawing.NoErrorFile(os.Open("./photos/res/like.png"))))
	}
	if icon != nil {
		drawing.DrawImage(drawing.ImageSlice{Rgb: formatted.Background.Rgb, Rect: bottomRight(formatted.Background.Rect)}, icon)
	}
	session.Text[i] = formatted
}

func bottomRight(i image.Rectangle) image.Rectangle {
	return image.Rectangle{Min: image.Point{i.Min.X + i.Dx()*8/10, i.Min.Y + i.Dy()*8/10}, Max: image.Point{i.Min.X + i.Dx()*9/10, i.Min.Y + i.Dy()*9/10}}
}
