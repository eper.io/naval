package main

import (
	"fmt"
	"gitlab.com/eper.io/naval/activation"
	drawing "gitlab.com/eper.io/naval/drawing"
	"gitlab.com/eper.io/naval/management"
	"gitlab.com/eper.io/naval/metadata"
	"gitlab.com/eper.io/naval/photos"
	"net/http"
)

// This document is Licensed under Creative Commons CC0.
// To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights
// to this document to the public domain worldwide.
// This document is distributed without any warranty.
// You should have received a copy of the CC0 Public Domain Dedication along with this document.
// If not, see https://creativecommons.org/publicdomain/zero/1.0/legalcode.

// A simple photo upload and sharing site
// Simply share the url of the photo list page including the private apikey.
func main() {
	drawing.SetupDrawing()
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if metadata.ActivationKey == "" {
			w.Header().Set("Location", "/index.html")
			w.WriteHeader(http.StatusTemporaryRedirect)
		} else {
			w.Header().Set("Location", "/activate.html")
			w.WriteHeader(http.StatusTemporaryRedirect)
		}
	})

	activation.SetupActivation()

	go func() {
		<-activation.Activated
		administrationKey := drawing.GenerateUniqueKey()

		management.SetupSiteManagement(administrationKey)
		activation.Activated <- administrationKey

		management.SetupSiteRoot()
		drawing.SetupUploads()
		photos.SetupPhotos()
	}()
	err := http.ListenAndServe(":7777", nil)
	if err != nil {
		fmt.Println(err)
	}
}
